import React from 'react';

export default class Candidature extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            applications: [],
            offers: [],
        }
    }



    componentDidMount() {
        
        fetch(`http://localhost:8000/api/useroffre`)
            .then(response => response.json())
            .then(json => this.setState({ applications: json }))
            .then(() =>
                fetch(`http://localhost:8000/api/offre`)
                    .then(response => response.json())
                    .then(json => this.setState({ offers: json }))
            )
    }




    render() {

        console.log("applications:", this.state.applications)
        console.log("offers:", this.state.offers)
        if (this.state.applications.length === 0 || this.state.offers.length === 0){
            return(<div></div>)
        }
        const getOffer = this.state.applications.map(element => +element.offre_id).map(id => {
            const offer = this.state.offers.find(off => off.id === id);
            if (offer) {
                console.log(offer);
                return offer
            }
        })
        console.log("mon get offer:",getOffer)
        



        let arr = getOffer.map(elm => (
            <li id='accueil-last-offer' key={elm.id} className="wid-30vw bg-white radius">

                <div className='px-1 py-2'>
                    <div className='d-flex t-between'>
                        <div className='my-auto'>
                            <p>applied on 23.02.2022</p>
                        </div>

                        <div className='d-none'>
                            <section className='d-flex z-20'>
                                <div className='starbox'>
                                    <div id='starbox-id'>
                                        <div className='star'>
                                            <div className='big'></div>
                                        </div>
                                    </div>
                                </div>
                                <div className='starbox z-75 p-absolute-transform'>
                                    <div id='starbox-new'>
                                        <div className='star'>
                                            <div className='small'></div>
                                        </div>
                                    </div>
                                </div>

                            </section>
                        </div>
                    </div>
                    <div>
                        <h1>{elm.title}</h1>
                    </div>
                    <div className='d-flex t-between mt-3'>
                        <p>{elm.ville}</p>
                    </div>
                </div>


            </li>))


        return (
            <div className='bg-grey-100 py-5'>
                <ul className='d-flex t-between px-5vw'>
                {arr}
                </ul>
            </div>
        )
    }
}
