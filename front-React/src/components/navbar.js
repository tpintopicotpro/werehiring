import '../App.css';
import React from 'react';

function Navbar(props) {
    console.log(props)
    function burgerKfc() {
        const kfc = document.getElementById('kfc');
        const mcdo = document.getElementById('mcdo');
        mcdo.style = "display: block"
        kfc.style = "display: none"
    }
    function burgerMcdo() {
        const kfc = document.getElementById('kfc');
        const mcdo = document.getElementById('mcdo');
        mcdo.style = "display: none"
        kfc.style = "display: block"
    }
    if (props.isLoged === false) {

        return (
            <div className='mb-7'>
                <div id='body-nav-1' className='d-flex t-between mx-5vw bg-white py-2'>
                    <div>
                        <h1>We Are Hiring</h1>
                    </div>
                    <div className='my-auto'>
                        <ul id='navUl' className='d-flex'>
                            <li>
                                <a onClick={() => props.callback("Accueil")}>Home</a>
                            </li>
                            <li>
                                <a onClick={() => props.callback("Offers")}>Offers</a>
                            </li>

                            <li>
                                <a onClick={() => props.callback("Login")}>Login</a>
                            </li>
                            <li>
                                <a onClick={() => props.callback("Register")}>Sign Up</a>
                            </li>
                            <li>
                                <a onClick={() => props.callback("Contact")}>Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id='body-nav-2' className='p-relative text-center bg-grey-100 py-2'>
                    <div>
                        <h1>We Are Hiring</h1>
                    </div>
                    <div id='kfc' onClick={burgerKfc} className='p-absolute-tr'>
                        <div className='smileXD-max'>
                            <div className='smileXD'></div>
                            <div className='smileXD-half'></div>
                            <div className='smileXD'></div>
                        </div>
                    </div>
                    <div id='mcdo'>
                        <div onClick={burgerMcdo} className='p-absolute-tr-mcdo'>
                            <div>X</div>
                        </div>
                        <ul className='text-start'>
                            <li><a onClick={() => props.callback("Accueil")}>Home</a></li>
                            <li><a onClick={() => props.callback("Offers")}>Offers</a></li>
                            <li><a onClick={() => props.callback("Login")}>Login</a></li>
                            <li><a onClick={() => props.callback("Register")}>Sign Up</a></li>
                            <li><a onClick={() => props.callback("Contact")}>Contact</a></li>
                        </ul>
                    </div>


                </div>

            </div>)
    }
    else {
        return (
            <div className='mb-7'>
                <div id='body-nav-1' className='d-flex t-between mx-5vw bg-white py-2'>
                    <div>
                        <h1>We Are Hiring</h1>
                    </div>
                    <div className='my-auto'>
                        <ul id='navUl' className='d-flex'>
                            <li>
                                <a onClick={() => props.callback("Accueil")}>Home</a>
                            </li>
                            <li>
                                <a onClick={() => props.callback("Offers")}>Offers</a>
                            </li>
                            <li>
                                <a onClick={() => props.callback("Candidature")}>My Application</a>
                            </li>
                            <li>
                                <a onClick={() => props.callback("Favoris")}>Favorites</a>
                            </li>
                            <li>
                                <a onClick={() => props.callback("Contact")}>Contact</a>
                            </li>
                            <li>
                                <a onClick={() => props.logOut(false)}>Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id='body-nav-2' className='p-relative text-center bg-grey-100 py-2'>
                    <div>
                        <h1>We Are Hiring</h1>
                    </div>
                    <div id='kfc' onClick={burgerKfc} className='p-absolute-tr'>
                        <div className='smileXD-max'>
                            <div className='smileXD'></div>
                            <div className='smileXD-half'></div>
                            <div className='smileXD'></div>
                        </div>
                    </div>
                    <div id='mcdo'>
                        <div onClick={burgerMcdo} className='p-absolute-tr-mcdo'>
                            <div>X</div>
                        </div>
                        <ul className='text-start'>
                            <li><a onClick={() => props.callback("Accueil")}>Home</a></li>
                            <li><a onClick={() => props.callback("Offers")}>Offers</a></li>
                            <li><a onClick={() => props.callback("Login")}>Login</a></li>
                            <li><a onClick={() => props.callback("Register")}>Sign Up</a></li>
                            <li><a onClick={() => props.callback("Contact")}>Contact</a></li>
                            <li><a onClick={() => props.logOut(false)}>Logout</a></li>
                        </ul>
                    </div>


                </div>
            </div>)
    }
}
export default Navbar;


