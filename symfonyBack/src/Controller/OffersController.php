<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Offre;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\OffreRepository;


    #[Route('/api/offres/{id}', name: 'offres')]
    class OffersController extends AbstractController
{
    
    public function show(int $id, ManagerRegistry $doctrine): Response
    {

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $offre = $doctrine->getRepository(Offre::class)->find($id);
        if (!$offre) {
            throw $this->createNotFoundException(
                'No product found for id '
            );
        }
        $jsonContent = $serializer->serialize($offre, 'json');    
        return new Response ($jsonContent);
    }
}

