import React from "react";
import LastOffer from "../pages/Accueil/last-offer";
import '../App.css';

export default class Favoris extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            favoriData: [],
            offerData: []
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount() {
        fetch(`http://localhost:8000/api/favori`)
            .then(response => {
                response.json().then(json => {
                    this.setState({ favoriData: json })
                    return json
                }).then(() => fetch('http://localhost:8000/api/offre')
                    .then(response => {
                        response.json().then(json => {
                            this.setState({ offerData: json })
                            return json
                        })
                    })
                )
            })
    }
    render() {

        console.log("favoriData:", this.state.favoriData)
        console.log("offerData:", this.state.offerData)
        if (this.state.favoriData.length === 0 || this.state.offerData.length === 0) {
            return (<div></div>)
        }
        const test = this.state.favoriData.map(element => +element.offer_id).map(id => {
            const offer = this.state.offerData.find(off => off.id === id)
            console.log("mon offid:", offer)
            if (offer) {
                return offer
            }
        })

        let sans_doublons = Array.from(new Set(test));


        let arr = sans_doublons.map(elm => (
            <li id='accueil-last-offer' key={elm.id} className="wid-30vw bg-white radius">



                <div className='px-1 py-2'>
                    <div className='d-flex t-between'>
                        <div className='my-auto'>
                            <p className='text-start'>applied on 23.02.2022</p>
                        </div>

                        <div className='d-none'>
                            <section className='d-flex z-20'>
                                <div className='starbox'>
                                    <div id='starbox-id'>
                                        <div className='star'>
                                            <div className='big'></div>
                                        </div>
                                    </div>
                                </div>
                                <div className='starbox z-75 p-absolute-transform'>
                                    <div id='starbox-new'>
                                        <div className='star'>
                                            <div className='small'></div>
                                        </div>
                                    </div>
                                </div>

                            </section>
                        </div>
                    </div>
                    <div>
                        <h1>{elm.title}</h1>
                    </div>
                    <div className='d-flex t-between mt-3'>
                        <p>{elm.ville}</p>
                    </div>
                </div>


            </li>))


        return (
            <div>
                {arr}
            </div>
        )
    }
}