import '../App.css';
import React, { Component } from 'react';

export default class Contacthome extends React.Component {

  constructor(props) {
    super(props)
  }
  render(){
    return(
      <div id='components-contacthome' className = "Contact" >
  
        <div className='d-flex t-between mx-20vw'>
          <div>
            <h3 className='my-2 h3'>Ready to get started?</h3>
            <p className='my-2 h4'>Contact us</p>
          </div>
          <div className='my-auto'>
            <button className='px-4 py-1 bg-white radius-1' onClick={() => this.props.handleClick("Contact")}><a>Contact Us</a></button>
          </div>
        </div>
  
  
      </div>)
  }

}