<?php

namespace App\Controller;

use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Security;
use App\Entity\Userz;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Offre;
use App\Repository\OffreRepository;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class ApiController extends AbstractController
{

    /**
     * @Route("/application/[offre_id]")
     */
    public function readOffer()
    {
        return $this->json(["message" => "The API successfully validated your access token."], Response::HTTP_OK);
    }

    /**
     * @Route("/contact")
     */
    public function contact()
    {
        return $this->json(["message" => "The API successfully recognized you as an admin."], Response::HTTP_OK);
    }
    /**
     * @param Security $security
     * @param JWTTokenManagerInterface $JWTManager
     * @return JsonResponse
     * @Route("/login_check")
     */
    public function getTokenUser(Security $security, JWTTokenManagerInterface $JWTManager,managerRegistry $doctrine)
    {
        $request = Request::createFromGlobals();
        $repository = $doctrine->getRepository(Userz::class)->findOneBy(['username'=>$request->get('email')]);

        if ($repository->getPassword() == $request->request->get('mdp')){
            return  $this->json(['token' => $JWTManager->create($repository)]);
        }
    }
    #[Route('/register', name: 'register')]
    public function register(ManagerRegistry $doctrine)
    {
        $request = Request::createFromGlobals();

        if ($request->request->get('mdp') == $request->request->get('comfirmMdp')){
        $entityManager = $doctrine->getManager();
        $newUser = new Userz();
        $newUser->setUsername($request->request->get('email'));
        $newUser->setPassword($request->request->get('mdp'));

        $entityManager->persist($newUser);
        $entityManager->flush();
        return new Response ('account created', Response::HTTP_OK);
        }else{
        return new Response('account creation failed', Response::HTTP_OK);
        }
    }

    // ICI SE TROUVE LES ROUTES POUR LES OFFRES : 
    
    #[Route('api/offres/{id}', name: 'offre')]
    public function showOffers(int $id, ManagerRegistry $doctrine): Response
    {

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $offre = $doctrine->getRepository(Offre::class)->find($id);
        if (!$offre) {
            throw $this->createNotFoundException(
                'No product found for id '
            );
        }
        $jsonContent = $serializer->serialize($offre, 'json');    
        return new Response ($jsonContent);
    }
    #[Route('api/offres', name: 'offres')]

    public function showOffer(ManagerRegistry $doctrine): Response
    {

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $offre = $doctrine->getRepository(Offre::class)->findAll();
        if (!$offre) {
            throw $this->createNotFoundException(
                'No product found for id '
            );
        }
        $jsonContent = $serializer->serialize($offre, 'json');    
        return new Response ($jsonContent);
    }
}
