import { isContentEditable } from '@testing-library/user-event/dist/utils';
import React from 'react';
import '../../App.css';
import ReadCard from '../../components/read'

export default class LastOffer extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
            data2: [],
            currentPage: 1,
            number: 9,
            favori: []
        }
        this.fetchPostFavorite = this.fetchPostFavorite.bind(this)
    }
    change(number) {
        let calc = this.state.currentPage * this.state.number
        this.setState({ currentPage: this.state.currentPage + number })
    }

    componentDidMount() {
        fetch('/api/offres')
            .then(response => {
                response.json().then(json => {
                    this.setState({ data2: json })
                    return json
                })
            });
        fetch(`/api/favori`)
            .then(response => {
                response.json().then(json => {
                    this.setState({ favori: json })
                    return json
                })
            });


    }
    fetchPostFavorite(event) {
        this.setState({ favorite: event.target.value })
        fetch(`/api/offres`)
        .then(response => response.json())
        .then(json => this.setState({ offers: json }))
        if(this.state.favorite !== undefined){
            fetch('/api/favori', {
            method: 'POST',
            body: (new URLSearchParams({
                user_id: 1,
                offer_id: this.state.favorite
            }))
        }
        )
    }
}

    render() {
        if (this.props.page === "Offers") {
            let tableau = []
            let calc = this.state.currentPage * this.state.number
            let max = Math.ceil(this.state.data2.length / this.state.number)
            let start = (this.state.currentPage - 1) * this.state.number
            for (let i = start; i < calc; i++) {
                if (this.state.currentPage > max && this.state.currentPage < 0) {
                    return tableau
                }
                else {
                    tableau.push(this.state.data2[i])
                }

            } 
            let fav = this.state.favori.map(elm => +elm.offer_id)
            let last_fav = Array.from(new Set(fav));
            
            let arr = tableau.map(elm => {

                if (elm !== undefined) {
                    return (
                        <li key={elm.id} className="wid-30vw bg-grey-100 radius">



                            <div className='px-1 py-2'>
                                <div className='d-flex t-between'>
                                    <div className='my-auto'>
                                        <div className='text-start'>23.02.2022</div>
                                    </div>
                                    <div id='input-star'>
                                    {this.props.onligne && last_fav.includes(elm.id) && <input type="checkbox" defaultChecked value={elm.id} onClick={this.fetchPostFavorite}></input>}
                                    { this.props.onligne && !last_fav.includes(elm.id) && <input type="checkbox" value={elm.id} onClick={this.fetchPostFavorite}></input>}
                                    </div>
                                </div>
                                <div>

                                    <h1>{elm.title}</h1>
                                    <div>{elm.content}</div>

                                </div>
                                <div className='d-flex t-between mt-3'>
                                    <div>Toulouse-CDI</div>
                                    <button id={elm.id} className='btn-style-none btn-style-go'><a id={elm.id} onClick={() => {
                                        this.props.handleClick("Read")
                                        this.props.getData(elm)
                                    }}> See More </a></button>
                                </div>
                            </div>
                        </li>
                    )
                }
            })

            return (<div>
                <div className='bg-grey-100 py-5'>
                    <h1 className='text-center h1 '>Offers</h1>
                </div>
                <ul id='accueil-offers' className='d-flex t-around px-5vw'>
                    {arr}
                </ul>

                <div id='pagination' className='d-flex t-between wid-50vw mx-auto mb-2'>
                    {this.state.currentPage > 1 ? <button id="previously" className="btn-style-none" onClick={() => this.change(-1)}>Previous Page</button> : <button id="previously2" className="btn-style-none bg-grey-100">Previous Page</button>}
                    {this.state.currentPage < max ? <button id="nextlyXD" className="btn-style-none" onClick={() => this.change(1)}>Next Page</button> : <button id="nextlyXD2" className="btn-style-none bg-grey-100">Next Page</button>}
                </div>
            </div>)
        }
        if (this.props.page === "Accueil") {
            let arr = this.props.data.map(elm => (
                <li id='accueil-last-offer' key={elm.id} className="wid-30vw bg-white radius">



                    <div className='px-1 py-2'>
                        <div className='d-flex t-between'>
                            <div className='my-auto'>
                                <div className='text-start'>23.02.2022</div>
                            </div>

                        </div>
                        <div className='p-relative'>
                            <div className='see-more-accueil'>
                                <h1>{elm.title}</h1>
                                <div className='t-justify'>{elm.content}</div>

                            </div>
                            <div className='j-end d-flex'><div></div><div className='t-transformers-see-more'>...</div><div></div></div>
                        </div>
                        <div className='d-flex t-between mt-3'>
                            <div>{elm.ville}</div>
                            <button id={elm.id} className='btn-style-none btn-style-go'><a id={elm.id} onClick={() => {
                                this.props.handleClick("Read")
                                this.props.getData(elm)
                            }}> See More </a></button>
                        </div>
                    </div>
                </li>))

            return (<div className='bg-grey-100 py-5'>

                <h1 className='text-center'>Last offers</h1>
                <ul className='d-flex t-between px-5vw'>
                    {arr}
                </ul>
            </div>)

        }

    }
}
