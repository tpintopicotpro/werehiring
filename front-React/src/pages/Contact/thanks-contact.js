import React from "react";

export default function Thanks(props) {
    return (
        <div className="thanks h-100vh-page text-center">
            <div className='bg-grey-100 py-5'>
                <h1 className='h1 text-center'>Contact Us</h1>
            </div>
            <div className="py-20">
                <h1>Thanks !</h1>
                <h2>Your message has been sent !</h2>
                <p>We thank you for your attention.</p>
                <p>We will respond as soon as possible.</p>
                <p>See you soon</p>
            </div>
        </div>
    )
}