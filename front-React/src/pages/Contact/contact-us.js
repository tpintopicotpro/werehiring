import '../../App.css';
import React from 'react';
import Thanks from './thanks-contact';

export default class FormContact extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            title: '',
            page: 'contact-us',
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleInputChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState(state => ({
            page: this.page = 'thanks'
        }));
    }

    render() {
        if (this.state.page === 'contact-us') {
            return (<div className='h-100vh-page'>
                <div className='bg-grey-100 py-5'>
                    <h1 className='h1 text-center'>Contact Us</h1>
                </div>
                <div className='py-5'>
                    <div>
                        <form id='form-contact-us' onSubmit={this.handleSubmit}>
                            <div className='mb-2'>
                                <div className='d-flex mb-1'>
                                    <div className='wid-20vw-page'></div>
                                    <div><label className='ml-1vw h3'>Your email address</label><br></br></div>
                                </div>
                                <div className='d-flex t-between'>
                                    <div className='wid-20vw-page'></div>
                                    <div><input className='wid-55vw-page h3 radius-1 px-2 py-1' required></input></div>
                                    <div className='wid-20vw-page'></div>
                                </div>
                            </div>

                            <div className='#'>
                                <div className='d-flex mb-1'>
                                    <div className='wid-20vw-page'></div>
                                    <div><label className='ml-1vw h3'>Your message</label><br></br></div>
                                </div>
                                <div className='d-flex t-between'>
                                    <div className='wid-20vw-page'></div>
                                    <div><textarea className='wid-55vw-page h3 radius-1 px-2 h-400px py-1' required></textarea></div>
                                    <div className='wid-20vw-page'></div>
                                </div>
                            </div>
                            <div className='d-flex t-between py-10'>
                                <div className='wid-20vw-page'></div>
                                <div><input type="submit" className='radius-1 py-1 c-white bg-black wid-15vw-page h5' value="Send"></input></div>
                                <div className='wid-20vw-page'></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>)
        }
        else if (this.state.page === 'thanks') {
            return (
                <Thanks />
            )
        }
    }

}