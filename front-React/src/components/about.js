import '../App.css';
import Navbar from '../components/navbar';
export default function About() {
    return (
            <div id='components-about' className="about">
                <div className='wid-30vw text-center py-5'>
                    <h1>About Us</h1>
                    <p className='h4'>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."</p>
                </div>
            </div>
        )
}