import React from "react";

export default function Thanks(props) {

    return (
        <div className="text-center">
            <div className='bg-grey-100 py-5'>
                <h1 className='h1 text-center'>Apply</h1>
            </div>
            <h1>Thanks !</h1>
            <p>Your application to "{props.title}" offer has been send</p>
            <p>We thank you for your application</p>
            <p>We will respond as soon possible</p>
            <p>See you soon</p>
        </div>
    )
}