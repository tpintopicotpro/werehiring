import '../App.css';
function Footer() {
    return (<footer className='bg-grey-100'>
        <div className='bor-b-black text-center mx-20vw py-2'><h1>We Are Hiring</h1></div>
        <div className='text-center py-2'><p>© 2022 - We Are Hiring</p></div>
    </footer>)
}
export default Footer;