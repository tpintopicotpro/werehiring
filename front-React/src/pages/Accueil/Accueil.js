import About from "../../components/about";
import Contacthome from '../../components/contacthome';
import Header from "../../components/header";
import React from 'react';
import Contact from '../../components/contact'
import LastOffer from "./last-offer";

export default class Accueil extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
        }
    }

    componentDidMount() {
        fetch('/api/offres')
            .then(response => {
                response.json().then(json => {
                    let arr_lastitem = []
                    let sized = json.length - 3

                    let size = json.reverse()

                    for (let i = 0; i < size.length - sized; i++) {
                        arr_lastitem.push(size[i])
                    }
                    this.setState({ data: arr_lastitem })

                    return arr_lastitem

                })
            })

    }


    render() {


        return (
            <div>
                <Header handleClick={this.props.handleClick} />
                <About />
                <LastOffer data={this.state.data} getData={this.props.getData} handleClick={this.props.handleClick} page={this.props.page} />
                <Contacthome handleClick={this.props.handleClick} />
            </div>
        )
    }



}
