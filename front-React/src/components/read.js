import { toHaveStyle } from "@testing-library/jest-dom/dist/matchers";
import React from "react";
import FormCandidature from "./form-candidature";
import LastOffer from "../pages/Accueil/last-offer"
export default class ReadCard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            page: 'Read'
        }
        this.handleClick = this.handleClick.bind(this)
        this.handleChange = this.handleChange.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);
    }
    handleClick(event) {
        if (this.props.isLoged === true) {
            this.setState({
                page: 'form-candidature'
            });
        }
    }

    handleRedirect() {
        this.props.offerRedirect("Offers")
    }
    handleChange(event) {
        event.preventDefault();
        this.setState({
            page: 'Offers'
        })
    }


    render() {
        if (this.state.page === 'Read') {
            return (
                <div id='offers'>
                    <div className='bg-grey-100 py-5'>
                        <h1 className='text-center h1'>{this.props.data.title}</h1>
                    </div>
                    <main className='py-5'>
                        <div className='px-5vw'>
                            <div className="d-flex t-between bold">
                                <div>
                                    <p>{this.props.data.fonction}</p>
                                </div>
                                <div>
                                    <p> {this.props.data.type} </p>
                                    <p> {this.props.data.ville} </p>
                                </div>
                            </div>
                            <p className="lign-h-6">{this.props.data.content}
                            </p>
                            <div className='mt-3'>
                                <p className='text-start'>Published on  02.23.2022</p>
                                <div className="text-center">
                                    <button className="mt-3 bg-black c-white h4 px-6 py-1 radius-1" onClick={this.handleClick}>Apply now !</button>
                                </div>
                                    <div className="text-center">
                                        <button id="go-back-1" className="mt-3 btn-style-none h4 px-6 py-1 radius-1" onClick={this.handleRedirect}>Back to Offers!</button>
                                    </div>
                                </div>
                        </div>
                    </main>
                </div>
            )
        }
        else if (this.state.page === 'form-candidature') {
            return (
                <FormCandidature data={this.props.data} id={this.props.data.id} isLoged={this.props.isLoged}/>
            )
        }

    }
}

